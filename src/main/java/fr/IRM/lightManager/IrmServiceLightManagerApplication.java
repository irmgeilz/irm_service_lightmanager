package fr.IRM.lightManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IrmServiceLightManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrmServiceLightManagerApplication.class, args);
	}

}
