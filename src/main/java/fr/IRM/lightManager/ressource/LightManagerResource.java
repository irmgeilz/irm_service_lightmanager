package fr.IRM.lightManager.ressource;

import java.io.IOException;

import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.resource.ContentInstance;
import org.eclispe.om2m.commons.client.Client;
import org.eclispe.om2m.commons.client.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.IRM.lightManager.utils.PostUtils;
import obix.Obj;
import obix.io.ObixDecoder;

@RestController
public class LightManagerResource {
	private Mapper mapper = new Mapper();
	private static String baseUrl = "http://localhost:8080/~/";
	private static String originator = "admin:admin";
	private static String deviceName = "light";
	
	//Function to retrieve the last state of the lamp in a specific room
	@GetMapping(value="/getLightState/{room}")
	public String getLightState(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;		
		try {
			resp = client.retrieve(baseUrl + room + "/" + room + "/" + deviceName + "/data/la",originator);
//			res = resp.getRepresentation();
			String s = resp.getRepresentation();
			ContentInstance cin = (ContentInstance)mapper.unmarshal(s);
			Obj o = ObixDecoder.fromString(cin.getContent());
			res = Integer.toString((int)o.get("Value").getInt());
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	//This service allow the customer to switch on the light (by posting a true boolean in the cin)
	@PostMapping(value="/switchLightOn/{room}")
	public String switchLightOn(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;
		try {
			resp = PostUtils.createCIN(PostUtils.generateStateCIN(room, this.deviceName, 1), baseUrl + room + "/" + room + "/" + deviceName + "/data", originator, client);
			res = resp.getRepresentation();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	//This service allow the customer to switch off the light (by posting a false boolean in the cin)
	@PostMapping(value="/switchLightOff/{room}")
	public String switchLightOff(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;
		try {
			resp = PostUtils.createCIN(PostUtils.generateStateCIN(room, deviceName, 0), baseUrl + room + "/" + room + "/" + deviceName + "/data", originator, client);
			res = resp.getRepresentation();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
}
