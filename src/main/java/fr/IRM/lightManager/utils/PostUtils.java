package fr.IRM.lightManager.utils;

import java.io.IOException;

import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.resource.ContentInstance;


import org.eclispe.om2m.commons.client.Client;
import org.eclispe.om2m.commons.client.Response;

import obix.Bool;
import obix.Int;
import obix.Obj;
import obix.Str;
import obix.io.ObixEncoder;

public class PostUtils {
	
	public static String generateStateCIN(String roomId, String typeAE, int deviceValue) {
		Str type = new Str(roomId);
		Str room = new Str(typeAE);
		Int value = new Int(deviceValue);
		Obj object = new Obj();
		object.add("RoomId", type);
		object.add("Type", room);
		object.add("Value", value);

		return ObixEncoder.toString(object);
	}
	
	public static Response createCIN(String content, String url, String originator, Client client) throws IOException {
		Response response = new Response();
		Mapper mapper = new Mapper();

		ContentInstance cin = new ContentInstance();
		cin.setContent(content);

		response = client.create(url, mapper.marshal(cin), originator, "4");
		return response;
	}
}
